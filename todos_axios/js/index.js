const BASE_URL ="https://635f4b2d3e8f65f283b01448.mockapi.io/";

var idEdited = null;

function fetchAllTodo(){
    turnOnLoading()
    axios({
        url:`${BASE_URL}/todos`,
        method:"GET",
    })
    .then
    (function(res){
turnOffLoading()
console.log('res todos: ', res.data);
renderTodoList(res.data)
    })
    .catch (function(err){
        turnOffLoading()
        
        console.log('err: ', err);
        
    });
}
fetchAllTodo();

function removeTodo(idTodo){
    turnOnLoading();
    axios({
        url:`${BASE_URL}/todos/${idTodo}`,
        method: "DELETE",
    })
    .then(function(res){
        turnOffLoading();
        fetchAllTodo();
        console.log('res: ', res);
    })
    .catch(function(err){
        console.log('err: ', err);

    })
}

function addTodo(){

    var data = layThongTinTuForm()
    var newTodo = {
        name: data.name,
        desc: data.desc,
        isComplete: false,
    };
    turnOnLoading();
    axios({
        url:`${BASE_URL}/todos`,
        method: "POST",
        data: newTodo,
    })
    .then(function(res){
        turnOffLoading();
        fetchAllTodo();
        
        console.log('res: ', res);
    })
    .catch(function(err){
        console.log('err: ', err);

    })
}

function editTodo(idTodo){
    turnOnLoading();
    axios({
        url:`${BASE_URL}/todos/${idTodo}`,
        method: "GET",
    })
    .then(function(res){
        turnOffLoading();
        //show thoong tin len form
        document.getElementById("name").value = res.data.name;
        document.getElementById("desc").value = res.data.desc;
        idEdited = res.data.id;
    })
    .catch(function(err){
        turnOffLoading();
        
        console.log('err: ', err);
    })
}

function updateTodo(){
let data = layThongTinTuForm();
axios({
    url:`${BASE_URL}/todos/${idEdited}`,
    method: "PUT",
    data: data,
})
.then(function(res){
    console.log('res: ', res);
    fetchAllTodo();
})
.catch(function(err){
    console.log('err: ', err);

})
}
